<?php

/**
 * @file
 * Adds a handler for Local videos to Video Embed Field.
 *
 * @see video_embed_field.api.php for more documentation
 */

/**
 * Implements hook_menu().
 */
function video_embed_local_menu() {
  $items = array();

  $items['video/%file/%'] = array(
    'type' => MENU_CALLBACK,
    'page callback' => 'video_embed_local_view_video',
    'page arguments' => array(1),
    'access callback' => TRUE,
  );

  return $items;
}

function video_embed_local_view_video($file) {
  $parts = explode('://', $file->uri);
  $args = array_merge(array($parts[0]), explode('/', $parts[1]));
  call_user_func_array('file_download', $args);
}

/**
 * Implements hook_video_embed_handler_info().
 */
function video_embed_local_video_embed_handler_info() {
  global $base_url;
  $parts = parse_url($base_url);
  $host = $parts['host'];
  if (stripos($host, 'www.') > -1) {
    $host = substr($host, 4);
  }
  $handlers = array();
  $handlers['local'] = array(
    'title' => 'Local Video',
    'function' => 'video_embed_local_handle_video',
    'thumbnail_function' => 'video_embed_local_handle_thumbnail',
    'thumbnail_default' => drupal_get_path('module', 'video_embed_local') . '/img/local.png',
    'data_function' => 'video_embed_field_handle_local_data',
    'form' => 'video_embed_local_form',
    'form_validate' => 'video_embed_field_handler_local_form_validate',
    'domains' => array(
      $host,
    ),
    'defaults' => array(
      'width' => 640,
      'height' => 360,
      'controls' => TRUE,
      'download_link' => FALSE,
      'download_text' => '',
    ),
  );

  return $handlers;
}

/**
 * Gets video data for a Local video URL.
 *
 * @param string $url
 *   A Local video URL to get data for
 *
 * @return array|bool
 *   An array of video data, or FALSE if unable to fetch data
 */
function video_embed_field_handle_local_data($url) {
  // Get Local video ID from URL.
  $id = _video_embed_local_get_video_id($url);

  if ($id) {
    return array('fid' => $id);
  }

  return FALSE;
}

/**
 * Defines the form elements for the Local videos configuration form.
 *
 * @param array $defaults
 *   The form default values.
 *
 * @return array
 *   The provider settings form array.
 */
function video_embed_local_form($defaults) {
  $form = array();

  $form['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Width'),
    '#description' => t('The width of the player.'),
    '#default_value' => $defaults['width'],
  );

  $form['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Height'),
    '#description' => t('The height of the player.'),
    '#default_value' => $defaults['height'],
  );

  $form['controls'] = array(
    '#title' => t('Controls'),
    '#type' => 'checkbox',
    '#default_value' => $defaults['controls'],
  );

  $form['download_link'] = array(
    '#title' => t('Download Link'),
    '#type' => 'checkbox',
    '#default_value' => $defaults['download_link'],
  );

  $form['download_text'] = array(
    '#title' => t('Download Link Text'),
    '#type' => 'textfield',
    '#default_value' => $defaults['download_text'],
  );

  return $form;
}

/**
 * Validates the form elements for the Local video configuration form.
 *
 * @param array $element
 *   The form element to validate.
 * @param array $form_state
 *   The form to validate state.
 * @param array $form
 *   The form to validate structure.
 */
function video_embed_field_handler_local_form_validate($element, &$form_state, $form) {
  video_embed_field_validate_dimensions($element);
}

/**
 * Handler for Local videos.
 *
 * @param string $url
 *   The video URL.
 * @param array $settings
 *   The settings array.
 *
 * @return string|bool
 *   The video iframe, or FALSE in case the ID can't be retrieved from the URL.
 */
function video_embed_local_handle_video($url, $settings) {
  // Check if the video exists.
  $fid = _video_embed_local_get_video_id($url);
  if (!$fid || !file_load($fid)) {
    return FALSE;
  }
  $class = drupal_html_id('video-embed-local-video');
  $js_settings = array();
  $js_settings['opts'] = array();
  $js_settings['controls'] = (bool)$settings['controls'];
  $js_settings['opts']['videoHeight'] = (int)$settings['height'];
  $js_settings['opts']['videoWidth'] = (int)$settings['width'];
  $video = array(
    '#attributes' => array(
      'src' => $url,
      'class' => $class,
    ),
    '#settings' => array(
      'download_link' => FALSE,
    ),
    '#attached' => array(
      'library' => array(array('mediaelement', 'mediaelement')),
      'js' => array(
        drupal_get_path('module', 'mediaelement') . '/mediaelement.js' => array(),
        0 => array('type' => 'setting', 'data' => array('mediaelement' => array('.' . $class => $js_settings))),
      ),
    ),
  );
  if ($settings['controls']) {
    $video['#attributes']['controls'] = 'controls';
  }
  $video['#theme'] = 'mediaelement_video';
  $video['#attributes']['height'] = $settings['height'];
  $video['#attributes']['width'] = $settings['width'];
  return $video;
}

/**
 * Gets the thumbnail url for Local videos.
 *
 * @param string $url
 *   The video URL.
 *
 * @return array
 *   The video thumbnail information.
 */
function video_embed_local_handle_thumbnail($url) {
  $id = _video_embed_local_get_video_id($url);
  $file = file_load($id);
  $thumbnail_path = explode('.', str_replace('videos/', 'videos/thumbnails/', $file->uri));
  array_pop($thumbnail_path);
  $thumbnail_path = implode('.', $thumbnail_path) . '_thumb.png';
  if (module_exists('phpvideotoolkit') && !file_exists($thumbnail_path)) {
    // TODO: This can take a very long time. Figure out a better way.
    global $user;
    $dir = dirname($thumbnail_path);
    file_prepare_directory($dir, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
    libraries_load('phpvideotoolkit');
    $path = drupal_realpath($file->uri);
    $video = new \PHPVideoToolkit\Video($path);
    $video->extractFrame(new \PHPVideoToolkit\Timecode(10))
      ->save(drupal_realpath($thumbnail_path));
    $thumb = new stdClass();
    $thumb->fid = NULL;
    $thumb->uri = $thumbnail_path;
    $thumb->filename = drupal_basename($thumbnail_path);
    $thumb->filemime = file_get_mimetype($thumb->uri);
    $thumb->uid = $user->uid;
    $thumb->status = FILE_STATUS_PERMANENT;
    file_save($thumb);
  }
  return array(
    'id' => $id,
    'url' => file_exists($thumbnail_path) ? file_create_url($thumbnail_path) : FALSE,
  );
}

/**
 * Helper function to get the Local video's id.
 *
 * @param string $url
 *   The video URL.
 *
 * @return string|bool
 *   The video ID, or FALSE in case the ID can't be retrieved from the URL.
 */
function _video_embed_local_get_video_id($url) {
  $without_filename = drupal_substr($url, 0, strrpos($url, '/'));
  return drupal_substr($without_filename, strrpos($without_filename, '/') + 1);
}

/**
 * Implements hook_field_widget_form_alter().
 */
function video_embed_local_field_widget_form_alter(&$element, &$form_state, $context) {
  if ($context['instance']['widget']['type'] === 'video_embed_field_video' && $context['instance']['settings']['allowed_providers']['local']) {
    $extensions = 'divx mkv mov 3gp 3g2 mp4 m4v rm f4v flv swf dir dcr asf wmv avi mpg mpeg ogg ogv webm mp3';
    $element_info = element_info('managed_file');
    $element['file'] = array(
      '#type' => 'managed_file',
      '#upload_validators' => array(
        'file_validate_extensions' => array($extensions),
      ),
      '#title' => t('Upload'),
      '#process' => array_merge($element_info['#process'], array('_video_embed_local_field_process')),
      '#default_value' => _video_embed_local_get_video_id($element['video_url']['#default_value']),
      '#upload_location' => 'public://videos/',
    );

    $element['#attributes']['class'][] = 'video-embed-local-fieldset';
    $element['#attached']['js'][drupal_get_path('module', 'video_embed_local') . '/js/video_embed_local.field.js'] = array();
  }
}

function _video_embed_local_field_process($element, &$form_state, $form) {
  global $base_url;
  $parents = $element['#parents'];
  $value = drupal_array_get_nested_value($form_state['values'], $parents);
  if ($value && $value['fid']) {
    array_pop($parents);
    $parents[] = 'video_url';
    $file = file_load($value['fid']);
    drupal_array_set_nested_value($form_state['values'], $parents, $base_url . '/video/' . $value['fid'] . '/' . $file->filename);
  }
  return $element;
}

/**
 * Implements hook_entity_insert().
 */
function video_embed_local_entity_insert($entity, $entity_type) {
  _video_embed_local_entity_insert_update($entity, $entity_type);
}

/**
 * Implements hook_entity_update().
 */
function video_embed_local_entity_update($entity, $entity_type) {
  _video_embed_local_entity_insert_update($entity, $entity_type);
}

/**
 * Helper function for hook_entity_insert() & hook_entity_update().
 */
function _video_embed_local_entity_insert_update($entity, $entity_type) {
  list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);
  $fields = field_read_fields(array(
    'type' => 'video_embed_field',
    'entity_type' => $entity_type,
    'bundle' => $bundle
  ));
  foreach ($fields as $field) {
    if (isset($entity->{$field['field_name']})) {
      foreach ($entity->{$field['field_name']} as $lang => $items) {
        foreach ($items as $delta => $item) {
          $handler = video_embed_get_handler($item['video_url']);
          if ($handler['name'] == 'local') {
            $fid = _video_embed_local_get_video_id($item['video_url']);
            $file = file_load($fid);
            if ($file && $file->status !== FILE_STATUS_PERMANENT) {
              $file->status = FILE_STATUS_PERMANENT;
              $usage = file_usage_list($file);
              // Add file usage.
              if (!isset($usage['video_embed_local'][$entity_type][$id])) {
                file_usage_add($file, 'video_embed_local', $entity_type, $id);
              }
              file_save($file);
            }
          }
        }
      }
    }
  }
}
